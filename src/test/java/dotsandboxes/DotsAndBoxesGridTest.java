package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class DotsAndBoxesGridTest {
    private DotsAndBoxesGrid grid;

    @BeforeEach
    public void setUp() {
        // Initialize the grid with a width of 4, height of 3, and 2 players for testing.
        grid = new DotsAndBoxesGrid(4, 3, 2);
    }

    @Test
    public void testBoxComplete() {
        // Draw a box by drawing all four sides
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        grid.drawVertical(1, 0, 1);

        // Check if the box is complete
        assertTrue(grid.boxComplete(0, 0), "Box (0, 0) should be complete after drawing all sides.");
    }

    @Test
    public void testBoxNotComplete() {
        // Draw only three sides of a box
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);

        // Check if the box is complete
        assertFalse(grid.boxComplete(0, 0), "Box (0, 0) should not be complete if any side is missing.");
    }

    @Test
    public void testDrawHorizontalLineAlreadyDrawn() {
        grid.drawHorizontal(0, 0, 1); // Draw a line
        // Attempt to draw the same line again should throw an exception
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1),
                "Drawing the same horizontal line twice should throw an IllegalStateException.");
    }

    @Test
    public void testDrawVerticalLineAlreadyDrawn() {
        grid.drawVertical(0, 0, 1); // Draw a line
        // Attempt to draw the same line again should throw an exception
        assertThrows(IllegalStateException.class, () -> grid.drawVertical(0, 0, 1),
                "Drawing the same vertical line twice should throw an IllegalStateException.");
    }
}
